from django.shortcuts import render
from .models import Receipt
from django.contrib.auth.decorators import login_required
# Create your views here.

@login_required
def receipt(request):
    receipt = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipt": receipt,


    }

    return render(request, 'receipts/detail.html', context)
